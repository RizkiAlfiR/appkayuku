<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'name', 'merk', 'size', 'description', 'price', 'quantity', 'image'
    ];
}
